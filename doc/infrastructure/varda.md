# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/cloud-upload.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Varda
| **Purpose** | Online storage, data synchronization |
| ------- | ------- |
| **Machine** | [Need to buy] Udoo x86 Advanced Plus + M.2 Key B 60mm SSD |
| **Hostname reason** | The Star-queen and the Queen of the Valar. |
| **IP** | 10.10.10.10 |
| **Notification** | varda.server@protonmail.com |
| **OS** | Debian, headless |
| **SSH keys** | _none_ |
| **Users** | root<br/>cm<br/>backup_nas<br/>wolfmah |
| **Main softwares** | NextCloudPi |

## General thoughts
KeePassXC file database need to reside inside NextCloud synchronized folders. (Database setup: Encryption Algorithm: Twofish: 256-bit; Key Derivation Function: Argon2; Transform rounds: ??; Memory Usage: ??; Parallelism: ??)

## Softwares
### NextCloudPi
