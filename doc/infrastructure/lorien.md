# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/gitlab.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Lorien
| **Purpose** | Git-repository hosting service, collaborative revision control |
| ------- | ------- |
| **Machine** | [Need to buy] Udoo x86 Ultra + M.2 Key B 60mm SSD |
| **Hostname reason** | Lord and Master of Dreams, Visions, and Desires, and Creator of the Oloré Mallé, or Path of Dreams. |
| **IP** | 10.10.10.11 |
| **Notification** | lorien.server@protonmail.com |
| **OS** | Debian, headless |
| **SSH keys** | _none_ |
| **Users** | root<br/>cm<br/>backup_nas<br/>wolfmah |
| **Main softwares** | GitLab |

## General thoughts
The installation of GitLab needs to be performed via the Omnibus package.

## Softwares
### GitLab
