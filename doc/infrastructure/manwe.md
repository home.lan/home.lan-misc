# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/wifi.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Manwe
| **Purpose** | WiFi access point |
| ------- | ------- |
| **Machine** | TP-Link Archer C7 |
| **Hostname reason** | The Wind-king and the King of the Valar. |
| **IP** | 10.2.2.2 |
| **Notification** | _none_ |
| **OS** | _none_ |
| **SSH keys** | _none_ |
| **Users** | ? |
| **Main softwares** | _none_ |

## General thoughts
Here, the hostname should be the SSID.
