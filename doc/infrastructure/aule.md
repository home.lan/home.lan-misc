# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/database.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Aule
| **Purpose** | Network attached storage (NAS) |
| ------- | ------- |
| **Machine** | Fractal Design Node 304 with i7-4770K @ 3.5GHz |
| **Hostname reason** | The Smith and the Lord of Earth and all that's underneath. |
| **IP** | 10.1.1.10 |
| **Notification** | aule.server@protonmail.com |
| **OS** | Debian or Ubuntu, headless |
| **SSH keys** | backup_nas-nas-to-nextcloud (logs / data / db)<br/>backup_nas-nas-to-gitlab (logs / data / db)<br/>backup_nas-nas-to-router (logs)<br/>backup_nas-nas-to-workstation (???) |
| **Users** | root<br/>cm<br/>backup_nas<br/>wolfmah |
| **Main softwares** | SnapRAID<br/>Borg?<br/>P7zip<br/>Nut (look into installing it for everybody?)<br/>Plex<br/>HandBrake? |

## General thoughts
I want it headless, because for now, my config doesn't boot if a screen isn't turned on. It's super annoying!

As long as the hardware doesn't change, could still be used for converting video if it's easy to do via command line.

Should have the ability to plug itself in all the other servers and do backup of their important stuff (ssh+rsync or borg).

## Softwares
### SnapRAID

### Borg

### P7zip

### Nut

### Plex

### HandBrake
