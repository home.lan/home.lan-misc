# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/desktop.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Olorin
| **Purpose** | Workstation |
| ------- | ------- |
| **Machine** | HDPLEX H1.S with i5-5675C @ 3.1GHz |
| **Hostname reason** | The wisest of the Maiar. He was a Maia of Manwë and Varda, a.k.a Mithrandir and Gandalf. |
| **IP** | 10.1.1.30 |
| **Notification** | _none_ |
| **OS** | Windows 10 |
| **SSH keys** | wolfmah-workstation-to-router<br/>wolfmah-workstation-to-nas<br/>wolfmah-workstation-to-nextcloud<br/>wolfmah-workstation-to-gitlab<br/>wolfmah-workstation-to-cm |
| **Users** | Administrator<br/>cm<br/>backup_nas<br/>wolfmah |
| **Main softwares** | ? |

## General thoughts

## Softwares
### ?
