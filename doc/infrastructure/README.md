# Infrastructure
All the PCs/servers hostname have been based on deities from Middle-Earth mythology. Servers have Valar names (gods) and PCs/laptops have Maiar names (angels). Their is a name encompassing both Valar and Maiar: Ainur (holy ones).

## References:
- Ainur: http://lotr.wikia.com/wiki/Ainur
- Valar: http://lotr.wikia.com/wiki/Valar
- Maiar: http://lotr.wikia.com/wiki/Maiar

## Legend
- SSH keys naming structure: `[username]_[server of origin]_to_[server to connect to]`

  Example: `cm_eru_to_tulkas`

  Enable the server Eru to connect to the server Tulkas, via the user `cm`. This implies that the user `cm` exist on the server Tulkas.

## Infrastructure
This is a list of all the servers / PCs / access point.
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/server.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Eru](eru.md): Configuration management, notification
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/server.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Tulkas](tulkas.md): Router (firewall/NAT/DNS/NTP)
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/database.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Aule](aule.md): NAS, Plex, automatic backup
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/cloud-upload.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Varda](varda.md): NextCloud
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/gitlab.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Lorien](lorien.md): GitLab
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/desktop.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Olorin](olorin.md): Workstation
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/wifi.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Manwe](manwe.md): Access point
- [<img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/unlock-alt.svg?sanitize=true" alt="Font Awesome - Server" width="16"> Mandos](mandos.md): Authentication

### From scratch
When building this infrastructure from scratch, the CM can't be the first server built, it will need a router in front. If the router doesn't exist, a minimal functioning router needs to be setup by hand (or script) and then the CM can be created and manage the router's configurations afterward.

### No auto-install
The way the configuration management (CM) server work, it can't managed uninstalled system; it requires a user with sudo power and a working SSH server.

So first, each server needs to be installed manually with a minimal SSH server working and a user that the CM can use. The way I see it, first, I'll need to install the router, so to have DHCP on the network, then the CM, then the others.

Building from scratch is kind of weird, but it won't happen often. Still, the process should be documented, and ideally, somewhat scripted.

### Diagrams
#### Network topology
The router have 4 NICs, each acting as a different zone:
1. Red for the Internet network. This is the connection from the ISP.
1. Green for the LAN network. It contains all sort of servers that should not be accessible from the Internet. No other network have access to LAN, but LAN can access all the others.
1. Blue for WiFi network. It contains only devices like phones, tablets and all sort of IoT. It can only access the Internet.
1. Orange for DMZ network. It contains servers that are meant to be accessible from the Internet. It can only access the Internet.
```mermaid
graph TB
  T --- V
  T --- L

  T --- M

  T --- E
  T --- A
  T --- P
  T --- O

  I -.- T

  subgraph DMZ
    V
    L
  end

  subgraph WiFi
    M
  end

  subgraph LAN
    E
    A
    P
    O
  end

  E[fa:fa-server Eru]
  T[fa:fa-server Tulkas]
  A[fa:fa-database Aule]
  V[fa:fa-cloud-upload Varda]
  L[fa:fa-gitlab Lorien]
  O[fa:fa-desktop Olorin]
  M[fa:fa-wifi Manwe]
  P[fa:fa-print Printer]
  I[fa:fa-cloud Internet]

  linkStyle 0 stroke:#ff9a00
  linkStyle 1 stroke:#ff9a00
  linkStyle 2 stroke:#00aaff
  linkStyle 3 stroke:#57d842
  linkStyle 4 stroke:#57d842
  linkStyle 5 stroke:#57d842
  linkStyle 6 stroke:#57d842
  linkStyle 7 stroke:#ff3d4e
```

#### Configuration management
Display which server act as the configuration manager and what servers it manages.
```mermaid
graph TB
  E --> V
  E --> L
  E --> T
  E --> A
  E --> O

  subgraph DMZ
    V
    L
  end

  subgraph LAN
    A
    O
  end

  E[fa:fa-server Eru]
  T[fa:fa-server Tulkas]
  A[fa:fa-database Aule]
  V[fa:fa-cloud-upload Varda]
  L[fa:fa-gitlab Lorien]
  O[fa:fa-desktop Olorin]
```

#### Automatic backup
Some backups are in need to be automated. The NAS will manage this and backup all sort of logs from the different servers. That way, logs on the servers can be pruned regularly, thus not contributing to clogging their drives.
```mermaid
graph TB
  A --> V
  A --> L
  A --> O
  A --> T

  subgraph DMZ
    V
    L
  end

  subgraph LAN
    O
  end

  T[fa:fa-server Tulkas]
  A[fa:fa-database Aule]
  V[fa:fa-cloud-upload Varda]
  L[fa:fa-gitlab Lorien]
  O[fa:fa-desktop Olorin]
```

#### Manual management
Some manual management will still be required, mostly for testing, manipulating data or creating backup archives. This is done from my personal computer.
```mermaid
graph TB
  O --> V
  O --> L
  O --> T
  O --> A
  O --> E

  subgraph DMZ
    V
    L
  end

  subgraph LAN
    E
    A
  end

  E[fa:fa-server Eru]
  T[fa:fa-server Tulkas]
  A[fa:fa-database Aule]
  V[fa:fa-cloud-upload Varda]
  L[fa:fa-gitlab Lorien]
  O[fa:fa-desktop Olorin]
```
