# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/server.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Tulkas
| **Purpose** | Router (DNS/Firewall) |
| ------- | ------- |
| **Machine** | SuperMicro CSE-101S with SuperMicro X11SBA-LN4F |
| **Hostname reason** | The Wrestler, the Champion of Valinor. |
| **IP** | dhcp (WAN / Red)<br/>10.1.1.1 (LAN / Green)<br/>10.2.2.1 (WiFi / Blue)<br/>10.10.10.1 (DMZ / Orange)<br/>10.100.100.? (IPMI) |
| **Notification** | tulkas.server@protonmail.com |
| **OS** | OpenBSD, headless |
| **SSH keys** | _none_ |
| **Users** | root<br/>cm<br/>backup_nas<br/>wolfmah |
| **Main softwares** | pf<br/>Unbound DNS<br/>Some NTP synchronisation<br/>fail2ban? |

## General thoughts
For a change, I want to try to configure the firewall with a pure pf configuration, instead of relying on pfSense. More information on anything OpenBSD can be found on Calomel.org website.

- Bootable OpenBSD CD: https://calomel.org/bootable_openbsd_cd.html
- OpenSSH Secure "how to" (ssh and sshd): https://calomel.org/openssh.html
- Sensorsd config (sensorsd.conf): https://calomel.org/sensorsd_config.html
- PF Config "how to" (pf.conf): https://calomel.org/pf_config.html
- Pfstat to graph PF logs (pfstat.conf): https://calomel.org/pfstat.html
- DNS for my local PCs (recursing, caching, validating, authoritative)
Unbound DNS Tutorial (unbound.conf): https://calomel.org/unbound_dns.html
- Chrony for FreeBSD (NTP): https://calomel.org/chrony_network_time.html
