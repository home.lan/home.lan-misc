# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/server.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Eru
| **Purpose** | Configuration management (CM) |
| ------- | ------- |
| **Machine** | [Need to buy (or reuse actual Nextcloud?)] Raspberry Pi 3 Model B+ |
| **Hostname reason** | The supreme deity of Arda. He was the single creator, above the Valar. |
| **IP** | 10.1.1.5 |
| **Notification** | eru.server@protonmail.com |
| **OS** | Debian, headless |
| **SSH keys** | cm-cm-to-router<br/>cm-cm-to-nas<br/>cm-cm-to-nextcloud<br/>cm-cm-to-gitlab<br/>cm-cm-to-workstation |
| **Users** | root<br/>cm<br/>wolfmah |
| **Main softwares** | Ansible<br/>protonmail-cli<br/>netdata |

## General thoughts
This server will be the configuration management (CM) server, the one at the helm, governing the configurations of all servers/PCs on the network.

## Softwares
### Ansible
The reason for using Ansible for the configuration management tool is because a lot of people are using it, it's simple and doesn't require anything other than OpenSSH and Python on hosts management by it.

### protonmail-cli
To be able to send server notifications with ProtonMail, my best bet, for now, is to use a Selenium stack that loads an hidden Firefox and inject all the right logins and simulate a user to send an email. This GitHub repo does the job: https://github.com/dimkouv/protonmail-cli.

Also, it would be best to have the other servers send their notifications to the CM via local network. That way, only one Selenium stack need to be maintained and the communication could be done via ZeroMQ, making it a perfect baby project to learn ZeroMQ.

### netdata
https://github.com/firehol/netdata

Allow to have a butt-ton of metrics about a server.
