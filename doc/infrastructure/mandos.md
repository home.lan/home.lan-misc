# <img src="https://raw.githubusercontent.com/encharm/Font-Awesome-SVG-PNG/master/black/svg/unlock-alt.svg?sanitize=true" alt="Font Awesome - Server" width="24"> Mandos
| **Purpose** | Identity management system |
| ------- | ------- |
| **Machine** | [Need to buy] Raspberry Pi 3 Model B+ |
| **Hostname reason** | The Doomsman and the Judge of the Dead. |
| **IP** | 10.1.1.3 |
| **Notification** | ? |
| **OS** | Debian, headless |
| **SSH keys** | ? |
| **Users** | ? |
| **Main softwares** | FreeIPA/Kerberos |

## General thoughts
Not used for now, but reserving the name.
