Infrastructure section contains:
- What physical server I have (or want).
- How everything is connected together.
- What purpose each server have.

Installation section contains:
- How to setup the whole network from scratch.
- What minimal configuration each server needs to have before CM can take over.
- Scripts to speed up the minimal configuration of each server.
